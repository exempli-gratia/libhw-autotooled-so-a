/*$off*/
/*$4 *******************************************************************************************************************
*
* DESCRIPTION:  shared stuff
*
* AUTHOR:       robert.berger@reliableembeddedsystems.com
*
* FILENAME:     lib/lib_hw.h
*
* REMARKS:      
*
* HISTORY:      001a,28 Aug 2015,rber    written
*
* COPYRIGHT:    (C) 2015 
*
 *********************************************************************************************************************** */
/*$on*/

#ifndef __LIB_HW_H__
#define __LIB_HW_H__ 1

/*$3 ===================================================================================================================
    $C                                             Included headers
 ======================================================================================================================= */


/* Included headers before this point */
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*$3 ===================================================================================================================
    $C                                                  Macros
 ======================================================================================================================= */

/* differentiate between public and private prototypes */
    #if defined(LIB_HW_PRIVATE_PROTOTYPES)
        #define EXTERN
    #else
        #define EXTERN  extern
    #endif

/*$3 ===================================================================================================================
    $C                                       Public function declarations
 ======================================================================================================================= */

    EXTERN void receive_byte (void);
    EXTERN void transmit_byte (void);

#undef EXTERN

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __HALF_DUPLEX_H__ */

/* 
 * EOF 
 */

